import './styles.scss';

import * as React from 'react';
import { Link } from 'react-router-dom';

import { OutboundLink } from 'react-ga';
import { ScholarlyArticle } from '../../../../../lib/interfaces/ScholarlyArticle';
import { Session } from '../../../../../lib/interfaces/Session';
import { convertToDoi, isDoiLink } from '../../../../../lib/utils/doi';
import { isArticleAuthor } from '../../../../../lib/utils/periodicals';
import { getOrcid } from '../../../../../lib/utils/person';
import { getFormattedDate, getFormattedDatetime } from '../../services/date';
import { OrcidButton } from '../orcidButton/component';
import { PageMetadata } from '../pageMetadata/component';
import { PlauditWidget } from '../plauditWidget/component';

interface Props {
  article: Partial<ScholarlyArticle>;
  url: string;
  slug: string;
  session?: Session;
}
interface State {}

export class ArticleOverview
  extends React.Component<
  Props,
  State
> {
  constructor(props: Props) {
    super(props);
  }

  public render() {
    return (
      <div itemScope={true} itemType="https://schema.org/ScholarlyArticle">
        <section className="hero is-medium is-primary is-bold">
          <div className="hero-body">
            <div className="container">
              <PageMetadata
                url={this.props.url}
                title={this.props.article.name}
                description={this.props.article.description}
                article={this.props.article}
              />
              <h1 itemProp="name" className="title">{this.props.article.name}</h1>
            </div>
          </div>
          <div className="hero-foot">
            <nav role="navigation" aria-label="Article navigation" className="tabs is-boxed is-right is-medium">
              <div className="container">
                <ul>
                  {this.renderSettingsLink()}
                </ul>
              </div>
            </nav>
          </div>
        </section>
        {this.renderSuggestion()}
        <section className="section">
          <div className="container">
            <div className="columns">
              <div className="column is-half-widescreen">
                <p itemProp="description" className="content">{this.props.article.description}</p>
              </div>
              <div className="column is-half-widescreen has-text-centered">
                {this.renderDownloadButton()}
                {this.renderPlauditWidget()}
              </div>
            </div>
          </div>
        </section>
        <section className="section">
          <div className="container">
            <div className="level">
              {this.renderPublicationVenue()}
              {this.renderAuthor()}
              {this.renderPublicationDate()}
            </div>
          </div>
        </section>
      </div>
    );
  }

  private renderDownloadButton() {
    if (!this.props.article.associatedMedia || this.props.article.associatedMedia.length < 1) {
      return this.renderSourceLink();
    }

    return (
      <OutboundLink
        itemProp="associatedMedia"
        itemScope={true}
        itemType="http://schema.org/MediaObject"
        className="button is-primary is-large"
        to={this.props.article.associatedMedia[0].contentUrl}
        download={this.props.article.associatedMedia[0].name}
        title="Read the full article"
        eventLabel="download-article"
        target="_blank"
      >
        <meta itemProp="contentUrl" content={this.props.article.associatedMedia[0].contentUrl} />
        <meta itemProp="license" content={this.props.article.associatedMedia[0].license} />
        <meta itemProp="name" content={this.props.article.associatedMedia[0].name} />
        Full article
      </OutboundLink>
    );
  }

  private renderPlauditWidget() {
    if (!this.props.article.sameAs || !isDoiLink(this.props.article.sameAs)) {
      return null;
    }

    const doi = convertToDoi(this.props.article.sameAs);
    // The following condition should always be `false` (thanks to `isDoiLink`),
    // so we cannot trigger it when running tests:
    /* istanbul ignore if */
    if (doi === null) {
      return null;
    }

    return (
      <div className="widgetContainer">
        <PlauditWidget doi={doi}/>
      </div>
    );
  }

  private renderSourceLink() {
    if (!this.props.article.sameAs) {
      return null;
    }

    return (
      <div className="content">
        <p>
          <button
            disabled={true}
            className="button is-large"
          >
            Article unavailable
          </button>
        </p>
        <p>
          This article is not freely available on Flockademic. It might be available&nbsp;
          <OutboundLink
            to={this.props.article.sameAs}
            title="View the source article"
            eventLabel="view-source-article"
          >
            here
          </OutboundLink>.
        </p>
      </div>
    );
  }

  private renderSettingsLink() {
    const author = this.props.article.author;
    if (
      typeof this.props.article.datePublished !== 'undefined' &&
      (!author || !this.props.session || !isArticleAuthor({ author }, this.props.session))
    ) {
      return null;
    }
    // TODO: Enable authors to claim external articles
    if (!this.props.article.identifier) {
      return null;
    }

    return (
      <li>
        <Link
          to={`/article/${this.props.article.identifier}/manage`}
          title="Configure your article's title, abstract, etc."
        >
          Article settings
        </Link>
      </li>
    );
  }

  private renderPublicationVenue() {
    const periodical = this.props.article.isPartOf;
    if (!periodical || !periodical.identifier || !periodical.name) {
      return null;
    }

    return (
      <div className="level-item">
        <div
          itemProp="isPartOf"
          itemScope={true}
          itemType="https://schema.org/Periodical"
        >
          <p className="heading">Published in</p>
          <p className="title">
            <Link
              to={`/journal/${periodical.identifier}`}
              title={`View other articles in: ${periodical.name}`}
              itemProp="url"
            >
              {periodical.name}
            </Link>
          </p>
        </div>
      </div>
    );
  }

  private renderAuthor() {
    if (
      !this.props.article.author ||
      this.props.article.author.length === 0 ||
      !this.props.article.author[0].sameAs ||
      !this.props.article.author[0].name
    ) {
      return null;
    }

    // The if statement above assures that the `sameAs` property is not undefined,
    // but since TypeScript can't deduce that we use the `!`:
    const websites = this.props.article.author[0].sameAs !;
    const orcid = getOrcid(websites);

    return (
      <div className="level-item">
        <div
          itemProp="author"
          itemScope={true}
          itemType="https://schema.org/Person"
        >
          <p className="heading">Author</p>
          <p className="title">
            <meta itemProp="sameAs" content={`https://orcid.org/${orcid}`} />
            <Link
              itemProp="name"
              to={`/profile/${orcid}`}
              title={`View all ${this.props.article.author[0].name}'s articles`}
            >
              {this.props.article.author[0].name}
            </Link>
          </p>
        </div>
      </div>
    );
  }

  private renderPublicationDate() {
    if (!this.props.article.datePublished) {
      return null;
    }

    return (
      <div className="level-item">
        <div>
          <p className="heading">Published on</p>
          <p className="title">
            <time
              itemProp="datePublished"
              dateTime={this.props.article.datePublished}
              title={getFormattedDatetime(this.props.article.datePublished)}
            >
              {getFormattedDate(this.props.article.datePublished)}
            </time>
          </p>
        </div>
      </div>
    );
  }

  private renderSuggestion() {
    const suggestion = this.getSuggestion();

    if (suggestion === null) {
      return null;
    }

    return (
      <section className="section">
        <div className="container">
          {suggestion}
        </div>
      </section>
    );
  }

  private getSuggestion() {
    // If it's an external article with no Open Access version available:
    if (
      !isInternal(this.props.article)
      && (!hasPdfAvailable(this.props.article))
    ) {
      if (!hasAccount(this.props.session)) {
        return (
          <div className="message is-danger content">
            <div className="message-body">
              Unfortunately, this article is not yet freely available. Is it yours?&nbsp;
              <OrcidButton>
                Sign in to share it!
              </OrcidButton>
            </div>
          </div>);
      }

      if (isAuthor(this.props.article, this.props.session)) {
        return (
          <div className="message is-danger content">
            <div className="message-body">
              This article is not yet freely available;&nbsp;
              {/* TODO: Allow linking a manuscript to this article */}
              <Link
                to={`/articles/new/${this.props.slug}`}
                title="Upload a manuscript"
              >
                share it now
              </Link>!
            </div>
          </div>);
      }

      if (this.props.article.author && this.props.article.author.length > 0) {
        return (
          <div className="message is-danger content">
            <div className="message-body">
              {/* tslint:disable-next-line:max-line-length */}
              Unfortunately, this article has not been made freely available. Encourage the author(s) to share their work by joining Flockademic!
            </div>
          </div>);
      }
    }

    if (isInternal(this.props.article) && (!this.props.article.name || !this.props.article.description)) {
      return (
        <div className="message is-warning content">
          <div className="message-body">
            Please configure this article's title and abstract in the&nbsp;
            <Link
              to={`/article/${this.props.article.identifier}/manage`}
              title="Configure your article's title, abstract, etc."
            >
              article settings
            </Link>.
          </div>
        </div>
      );
    }

    if (
      !isInternal(this.props.article)
      && !this.props.article.description
      && this.props.session && isAuthor(this.props.article, this.props.session)
      && hasPdfAvailable(this.props.article)
    ) {

      return (
        <div className="message is-info content">
          <div className="message-body">
            Tip:&nbsp;
            <Link
              to={`/articles/new/${this.props.slug}`}
              title="Submit this article to Flockademic"
            >
              add an abstract
            </Link>
            &nbsp;to make this article easier to find.
          </div>
        </div>
      );
    }

    if (!hasPdfAvailable(this.props.article)) {
      return (
        <div className="message is-warning content">
          <div className="message-body">
            Please upload your article in the&nbsp;
            <Link
              to={`/article/${this.props.article.identifier}/manage`}
              title="Configure your article's title, abstract, etc."
            >
              article settings
            </Link>.
          </div>
        </div>
      );
    }

    if (isInternal(this.props.article) && !this.props.article.datePublished) {
      return (
        <div className="message is-warning content">
          <div className="message-body">
            Your article has not been submitted to the journal yet. You can do so in the&nbsp;
            <Link
              to={`/article/${this.props.article.identifier}/manage`}
              title="Configure your article's title, abstract, etc."
            >
              article settings
            </Link>.
          </div>
        </div>
      );
    }

    return null;
  }
}

function isInternal(article: Partial<ScholarlyArticle>) {
  return typeof article.identifier === 'string';
}

function hasPdfAvailable(article: Partial<ScholarlyArticle>) {
  return article.associatedMedia && article.associatedMedia.length > 0;
}

function hasAccount(session?: Session): session is Session & { account: Account } {
  return typeof session !== 'undefined' && typeof session.account !== 'undefined';
}

function isAuthor(article: Partial<ScholarlyArticle>, session: Session) {
  if (!article.author || article.author.length === 0) {
    return false;
  }

  const author = article.author[0];

  return (
    session.account && session.account.orcid
    && article.author && author.sameAs
    && session.account.orcid === getOrcid(author.sameAs)
  );
}
