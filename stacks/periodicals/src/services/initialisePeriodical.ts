import { NewPeriodical } from '../../../../lib/interfaces/endpoints/periodical';
import { Session } from '../../../../lib/interfaces/Session';
import { Database } from '../../../../lib/lambda/middleware/withDatabase';

// This file is ignored for test coverage in the Jest configuration
// since it is merely a translation of Javascript objects to SQL queries.
export async function initialisePeriodical(
  database: Database,
  session: Session,
  periodical: NewPeriodical,
): Promise<void> {
  return database.tx((t) => {
    const queries = [
      t.none(
        'INSERT INTO periodicals (identifier, uuid, name, headline, description, creator_session)'
          // tslint:disable-next-line:no-invalid-template-strings
          + ' VALUES (${identifier}, ${identifier}, ${name}, ${headline}, ${description}, ${creator_session})',
        {
          ...periodical,
          creator_session: session.identifier,
        },
      ),
    ];

    if (session.account) {
      queries.push(t.none(
        // tslint:disable-next-line:no-invalid-template-strings
        'INSERT INTO periodical_creators (periodical, creator) VALUES (${periodical}, ${creator})',
        { periodical: periodical.identifier, creator: session.account.identifier },
      ));

      periodical.creator = { identifier: session.account.identifier };
    }

    return t.batch(queries);
  });
}
